<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Restaurant;

class RestaurantController extends Controller
{
    public function store(Request $request){
        $validatedData = $request->validate([
            'name' => 'required|min:1|max:64'
        ]);
        // dd($request);
        $restaurant = new Restaurant();
        $restaurant->name = $request->input('name');
        $fileName = time().'_'.$request->file->getClientOriginalName();
        $filePath = $request->file('file')->move(public_path('/uploads'), $fileName, 'public');
        $restaurant->image=$fileName;
        $restaurant->address=$request->input('address');
        $restaurant->contact_no=$request->input('contact_no');
        $restaurant->email=$request->input('email');
        $restaurant->description=!blank($request->input('description')) ? $request->input('description') :'' ;
        $restaurant->save();
        return response()->json( array('success' => true) );
    }

    public function index(){
        $data=Restaurant::all();
        return response()->json(compact('data'));
    }

    public function delete($id){
        $data=Restaurant::find($id);
        if($data){
            $data->delete();
        }
        return response()->json(['status'=>'success']);
    }

    public function update($id,Request $request){
        // dd($id);
        $data=Restaurant::find($id);
        // dd($data);
        if($data){
            $fileName = time().'_'.$request->file->getClientOriginalName();
            // dd($fileName);
            $filePath = $request->file('file')->move(public_path('/uploads'), $fileName, 'public');
            // File::delete('/public/uploads/'.$data->image);

            Restaurant::where('id',$id)->update([
                'name'=>$request->name,
                'address'=>$request->address,
                'image'=>$fileName
            ]);
        }
        return response()->json(['status'=>'success']);
    }

    public function updateStatus($id){
        $user = Restaurant::find($id);
        if($user->status ==0){
            $user->update(['status'=>1]);
        }else{
            $user->update(['status'=>0]);
        }
        return response()->json( ['status' => 'success'] );

    }
}
