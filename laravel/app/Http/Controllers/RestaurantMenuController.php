<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\RestaurantMenu;
use App\Models\Restaurant;

class RestaurantMenuController extends Controller
{
    public function index(){
        // $data=RestaurantMenu::select('restaurant_menus.*','restaurant_menus.name as name','restaurants.name as restuarant')
        // ->join('restaurants',function($join){
        //     $join->on('restaurant_menus.restaurant_id','=','restaurants.id');
        // })->get();
        $data=RestaurantMenu::all();
        return response()->json(compact('data'));
    }
   
    public function getRestaurant(){
        $data=Restaurant::all();
        $category=RestaurantMenu::all();
        return response()->json(compact('data','category'));
    }

    public function store(Request $request){
        $validatedData = $request->validate([
            'name' => 'required|min:1|max:64'
        ]);
        
        $restaurantMenu = new RestaurantMenu();
        $restaurantMenu->name = $request->input('name');
        // $restaurantMenu->restaurant_id=$request->input('restaurant_id');
        $restaurantMenu->save();
        return response()->json( array('success' => true) );  

    }
    public function delete($id){
        $data=RestaurantMenu::find($id);
        if($data){
            $data->delete();
        }
        return response()->json(['status'=>'success']);
    }

    public function update($id,Request $request){
        $data=RestaurantMenu::find($id);
        if($data){
            RestaurantMenu::where('id',$id)->update([
                'name'=>$request->name,
                // 'restaurant_id'=>$request->restaurant_id
            ]);
        }
        return response()->json(['status'=>'success']);
    }


}
