<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\RestaurantMenu;
use App\Models\Item;
use SimpleSoftwareIO\QrCode\Facades\QrCode;

class ItemController extends Controller
{
    public function getMenuCategory(){
        $data=RestaurantMenu::all();
        // dd($data);
        return response()->json(compact('data'));
    }

    public function create(Request $request){
        $validatedData = $request->validate([
            'name' => 'required|min:1|max:64',
            'category_id'=>'required',
            'restaurant_id'=>'required',
            'price'=>'required'
        ]);
        // dd($request);
        $item = new Item();
        $item->name = $request->input('name');
        if(!blank($request->file)){
            $fileName = time().'_'.$request->file->getClientOriginalName();
            $filePath = $request->file('file')->move(public_path('/uploads'), $fileName, 'public');
            $item->image=$fileName;
        }else{
            $item->image='';
        }
    
        $item->category_id=$request->input('category_id');
        $item->restaurant_id=$request->input('restaurant_id');
        $item->price=$request->input('price');
        $item->status=0;
        $item->description=$request->input('description');
        $item->service_size_unit=$request->input('service_size_unit');
        $item->service_size_value=$request->input('service_size_value');
        $item->save();
        return response()->json( array('success' => true) );
    }

    public function index($id){
        $all_data=Item::where('restaurant_id',$id)->get();
        $data = $all_data->transform(function ($item) {
            $item->restaurant=\App\Models\Restaurant::where('id',$item->restaurant_id)->value('name');
            $item->category=\App\Models\RestaurantMenu::where('id',$item->category_id)->value('name');
            return $item;
        });
        return response()->json(compact('data'));
    }

    public function updateStatus($id){
        $user = Item::find($id);
        if($user->status ==0){
            $user->update(['status'=>1]);
        }else{
            $user->update(['status'=>0]);
        }
        return response()->json( ['status' => 'success'] );

    }

    public function qr_generate(){
        $data=\App\Models\Restaurant::where('id',7)->first();
        $qrcode = QrCode::size(400)->generate($data->name);
        return response()->json(['status'=>'success','qrcode'=>$qrcode]);

    }

    public function delete($id){
        $data=Item::find($id);
        if($data){
            $data->delete();
        }
        return response()->json(['status'=>'success']);
    }

    public function getMenuCategoryList(){
        $data=\App\Models\RestaurantMenu::all();
        return response()->json(compact('data'));
    }


    // public function generate ($id)
    // {
    //     $data = Data::findOrFail($id);
    //     $qrcode = QrCode::size(400)->generate($data->name);
    //     return view('qrcode',compact('qrcode'));
    // }

    public function update($id,Request $request){
        $data=Item::find($id);
        if($data){
            $fileName = time().'_'.$request->file->getClientOriginalName();
            $filePath = $request->file('file')->move(public_path('/uploads'), $fileName, 'public');

            Item::where('id',$id)->update([
                'name'=>$request->name,
                'category_id'=>$request->category_id,
                'image'=>$fileName,
                'restaurant_id'=>$request->restaurant_id,
                'price'=>$request->price
            ]);
        }
        return response()->json(['status'=>'success']);
    }
}
