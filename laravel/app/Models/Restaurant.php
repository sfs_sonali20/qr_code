<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Restaurant extends Model
{
    protected $table = 'restaurants';
    protected $fillable =['name','image','address','status','contact_no','email','description'];
    // public $timestamps = false; 
}
