<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RestaurantMenu extends Model
{
    protected $table="restaurant_menus";
    protected $fillable=['name','restaurant_id','status'];
}
